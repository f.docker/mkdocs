ARG IMAGE_TAG
FROM squidfunk/mkdocs-material:$IMAGE_TAG

RUN apk update && \
    apk upgrade && \
    apk add --no-cache build-base linux-headers zeromq-dev

RUN pip install \
        mkdocs-material \
        mknotebooks \
        mkdocs-exclude \
        mkdocs-awesome-pages-plugin \
        pymdown-extensions
