PROJECT_NAME := mkdocs
PROJECT_NAMESPACE ?= f.docker

IMAGE_TAG := 6.2.4

REGISTRY ?= registry.gitlab.com
REGISTRY_IMAGE := $(REGISTRY)/$(PROJECT_NAMESPACE)/$(PROJECT_NAME)
REGISTRY_IMAGE_TAG := $(REGISTRY_IMAGE):$(IMAGE_TAG)
REGISTRY_IMAGE_LATEST_TAG := $(REGISTRY_IMAGE):latest

all: image

.PHONY: image
.ONESHELL:
image:
	docker build \
		--build-arg IMAGE_TAG=$(IMAGE_TAG) \
		-t $(REGISTRY_IMAGE_TAG) \
		-t $(REGISTRY_IMAGE_LATEST_TAG) \
		.

.PHONY: push
push:
	docker login $(REGISTRY)
	docker push -a $(REGISTRY_IMAGE)

.PHONY: push-name
push-name:
	@echo "$(REGISTRY_IMAGE_TAG)"

.PHONY: version
version:
	@echo $(IMAGE_TAG)
